'use strict';

let gulp = require('gulp');
let sass = require('gulp-sass');

gulp.task('sass', function() {
	gulp.src([
		'./src/*.scss',
		'./src/app/*.scss',
		'./src/app/components/**/*.scss'
	])
		.pipe(sass())
		.pipe(gulp.dest(function(f) {
			return f.base;
		}));
});

gulp.task('default', ['sass'], function() {
	gulp.watch('./src/app/components/**/*.scss', ['sass']);
	gulp.watch('./src/styles/**/*.scss', ['sass']);
});