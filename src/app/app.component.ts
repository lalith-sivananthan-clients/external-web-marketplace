import { Component, ViewEncapsulation } from '@angular/core';

import { AccountModel } from '@app/models/local/account/account.classes';

@Component({
	selector: 'sprout-root',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './app.component.html',
	styleUrls: [
		'./app.component.css'
	],
})
export class AppComponent {

	constructor(
		public accountModel: AccountModel
	) {
	}

}
