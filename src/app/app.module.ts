//

import { CookieService } from 'ngx-cookie-service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';

import { NgModule, Injector, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//

import { Routing, AppRoutingProviders } from './app.routes';

// models : local
import { AccountModel } from '@app/models/local/account/account.classes';

// models : api
import { AuthenticationAPI } from '@app/models/api/authentication/authentication.classes';
import { ConnectAPI } from '@app/models/api/connect/connect.classes';
import { AccountAPI } from '@app/models/api/account/account.classes';
import { ShopAPI } from '@app/models/api/shop/shop.classes';
import { ShopProductAPI } from '@app/models/api/shop-product/product.classes';
import { SearchAPI } from '@app/models/api/search/search.classes';
import { CartAPI } from '@app/models/api/cart/cart.classes';

// directives
import { ActiveDirective } from '@app/directives/active.directive';
import { AutoHeightDirective } from '@app/directives/auto-height.directive';
import { FocusDirective } from '@app/directives/focus.directive';
import { ValidatorFieldDirective } from '@app/directives/validator-field.directive';
import { ValidatorFormDirective } from '@app/directives/validator-form.directive';
import { FileDropDirective } from '@app/directives/file-drop.directive';
import { FileSelectDirective } from '@app/directives/file-select.directive';
import { FileClickDirective } from '@app/directives/file-click.directive';

// services : core
import { InjectorService } from '@app/services/core/injector/injector.classes';
import { StorageCookieService } from '@app/services/core/storage/cookie.classes';
import { StorageLocalService } from '@app/services/core/storage/local.classes';
import { LanguageService } from '@app/services/core/language/language.classes';
import { MessagerService } from '@app/services/core/messenger/messenger.classes';
import { ValidatorService } from '@app/services/core/validator/validator.classes';
import { PaginationService } from '@app/services/core/pagination/pagination.classes';
import { ProductTaxonomyService } from '@app/services/core/helpers/product-taxonomy.classes';

// services : interceptors
import { ResponseHttpInterceptor } from '@app/services/core/interceptors/interceptor.classes';

// services : guard
import { AuthenticationGuard } from '@app/services/guards/authentication/authentication.classes';

// components
import { AppComponent } from '@app/app.component';
import { DashboardComponent } from '@app/components/dashboard/dashboard.component';
import { IntroComponent } from '@app/components/intro/intro.component';

// components : _shared
import { SharedUploaderComponent } from '@app/components/_shared/uploader/uploader.component';
import { SharedPaginationComponent } from '@app/components/_shared/pagination/pagination.component';

// components : message
import { MessageGlobalComponent } from '@app/components/message/global/global.component';
import { MessageFormComponent } from '@app/components/message/form/form.component';

// components : nav
import { NavTopSignedInComponent } from '@app/components/nav/top-signed-in/top-signed-in.component';

// components : search
import { ShopSingleComponent } from '@app/components/shop/single/single.component';
import { ShopListComponent } from '@app/components/shop/list/list.component';
import { ShopSingleProductListComponent } from '@app/components/shop/single/product/list/list.component';

import { ProductSingleComponent } from '@app/components/product/single/single.component';
import { ProductListComponent } from '@app/components/product/list/list.component';

// components : cart
import { CartSingleComponent } from '@app/components/cart/single/single.component';
import { CartSingleProductComponent } from '@app/components/cart/single/product/product.component';

import { CartCheckoutComponent } from '@app/components/cart/checkout/checkout.component';

import { CartAddComponent } from '@app/components/cart/add/add.component';

@NgModule({
    declarations: [
        AppComponent,
        IntroComponent, DashboardComponent,

        SharedUploaderComponent, SharedPaginationComponent,

        MessageGlobalComponent, MessageFormComponent,

        NavTopSignedInComponent,

        ShopSingleComponent, ShopListComponent, ShopSingleProductListComponent,

        ProductSingleComponent, ProductListComponent,

        CartSingleComponent, CartSingleProductComponent, CartCheckoutComponent, CartAddComponent,

        ActiveDirective, AutoHeightDirective, FocusDirective, ValidatorFieldDirective, ValidatorFormDirective,
        FileDropDirective, FileSelectDirective, FileClickDirective
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        Routing,
        FormsModule, ReactiveFormsModule,
        FontAwesomeModule
    ],
    providers: [
        CookieService,

        {provide: HTTP_INTERCEPTORS, useClass: ResponseHttpInterceptor, multi: true},
        AppRoutingProviders,

        AccountModel,

        InjectorService, StorageCookieService, StorageLocalService, LanguageService, MessagerService, ValidatorService, PaginationService,
        ProductTaxonomyService,

        AuthenticationGuard,

        AuthenticationAPI, ConnectAPI, AccountAPI, ShopAPI, ShopProductAPI, SearchAPI, CartAPI
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
    constructor(private injector: Injector) {
        InjectorService.Injector = this.injector;

        library.add(fas, far, fab);
    }
}
