import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// services : guards
import { AuthenticationGuard } from '@app/services/guards/authentication/authentication.classes';

// components
import { DashboardComponent } from '@app/components/dashboard/dashboard.component';

// components : search
import { ShopSingleComponent } from '@app/components/shop/single/single.component';
import { ShopListComponent } from '@app/components/shop/list/list.component';
import { ShopSingleProductListComponent } from '@app/components/shop/single/product/list/list.component';

import { ProductSingleComponent } from '@app/components/product/single/single.component';
import { ProductListComponent } from '@app/components/product/list/list.component';

import { CartSingleComponent } from '@app/components/cart/single/single.component';
import { CartCheckoutComponent } from '@app/components/cart/checkout/checkout.component';

const appRoutes: Routes = [
    // {
    //     path: 'dashboard',
    //     component: DashboardComponent,
    //     canActivate: [AuthenticationGuard]
    // },
    {
        path: 'supplier',
        children: [
            {
                path: ':storeID',
                children: [
                    {
                        path: '',
                        component: ShopSingleComponent,
                        canActivate: [AuthenticationGuard]
                    },
                    {
                        path: 'products',
                        children: [
                            {
                                path: '',
                                component: ShopSingleProductListComponent,
                                canActivate: [AuthenticationGuard]
                            },
                            {
                                path: ':page',
                                component: ShopSingleProductListComponent,
                                canActivate: [AuthenticationGuard]
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        path: 'suppliers',
        children: [
            {
                path: '',
                component: ShopListComponent,
                canActivate: [AuthenticationGuard]
            },
            {
                path: ':page',
                component: ShopListComponent,
                canActivate: [AuthenticationGuard]
            }
        ]
    },
    {
        path: 'product/:productID',
        component: ProductSingleComponent,
        canActivate: [AuthenticationGuard]
    },
    {
        path: 'products',
        children: [
            {
                path: '',
                component: ProductListComponent,
                canActivate: [AuthenticationGuard]
            },
            {
                path: ':page',
                component: ProductListComponent,
                canActivate: [AuthenticationGuard]
            }
        ]
    },
    {
        path: 'cart',
        children: [
            {
                path: '',
                component: CartSingleComponent,
                canActivate: [AuthenticationGuard]
            },
            {
                path: 'checkout',
                component: CartCheckoutComponent,
                canActivate: [AuthenticationGuard]
            },
        ]

    },
    {
        path: '**',
        redirectTo: 'suppliers'
    }
];

export const AppRoutingProviders: any[] = [];
export const Routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
