import Cropper from 'cropperjs';

import { Component, ViewEncapsulation, Input, Output, EventEmitter, ViewChild, ElementRef, OnInit, OnDestroy, AfterViewInit } from '@angular/core';

import { IValidatorImage, IValidatorImageRules } from '@app/services/core/validator/image.interfaces';
import { ImageFindDimension, ImageValidateDimension } from '@app/services/core/validator/image.functions';

import { IImageCrop } from '@app/components/_shared/uploader/uploader.interfaces';
import { UploadingStatusEvent } from '@app/components/_shared/uploader/uploader.variables';

@Component({
	selector: 'sprout-shared-uploader',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './uploader.component.html',
	styleUrls: [
		'./uploader.component.css'
	]
})
export class SharedUploaderComponent implements OnInit, OnDestroy, AfterViewInit {

	// input

	@Input()
	sproutFile: File;

	@Input()
	sproutFiles: Set<File>;

	@Input()
	sproutFileCrop: IImageCrop;

	@Input()
	sproutFileMultiple: boolean;

	@Input()
	sproutFileRules: IValidatorImageRules;

	// output

	@Output()
	sproutFileChange: EventEmitter<File> = new EventEmitter<File>();

	@Output()
	sproutFilesChange: EventEmitter<Set<File>> = new EventEmitter<Set<File>>();

	@Output()
	sproutFileCropChange: EventEmitter<IImageCrop> = new EventEmitter<IImageCrop>();

	//
	@Output()
	sproutTriggerUpload: EventEmitter<any> = new EventEmitter<any>();

	// child

	@ViewChild('inputSelector')
	inputSelector: ElementRef;

	@ViewChild('editorImage')
	editorImage: ElementRef;

	@ViewChild('defaultImage')
	defaultImage: ElementRef;

	// _ events

	private _subscribeUploadingStatusEvent: any;

	//
	public showEditor: boolean;
	public editorStyle: {};

	//

	public cropper: Cropper;

	constructor() {
		this.sproutFile = null;
		this.sproutFiles = new Set();

		this.showEditor = false;
	}

	ngOnInit() {
		this._subscribeUploadingStatusEvent = UploadingStatusEvent.subscribe((data) => {
			console.log(data);
		});
	}

	ngOnDestroy() {
		this._subscribeUploadingStatusEvent.unsubscribe();
	}

	ngAfterViewInit() {
		let image = this.defaultImage.nativeElement as HTMLImageElement;

		this.editorStyle = {
			'width': '100%',
			'max-height': image.width + 'px',
			'float': 'left',
			'overflow': 'hidden'
		};
	}

	//

	public changeFile() {
		if (this.inputSelector.nativeElement.files.length === 0) {
			if (this.sproutFile !== null) {
				this.showEditor = true;
				return;
			}

			this.showEditor = false;
			return;
		}

		if (this.sproutFileMultiple === true) {
			const files: { [key: string]: File } = this.inputSelector.nativeElement.files;

			for (let key in files) {
				if (!isNaN(parseInt(key, 10))) {
					this.sproutFiles.add(files[key]);
				}
			}
		} else {
			this.showEditor = true;
			this._loadImage();
		}
	}

	public reset() {
		this.editorImage.nativeElement.src = null;
		this.cropper.clear();
		this.cropper.destroy();
	}

	public openUploader(event, element: HTMLElement) {
		element.click();
	}

	// trigger

	public triggerUpload() {
		let data = this.cropper.getData(true);

		this.sproutFileCrop = {
			XOffset: data.x,
			YOffset: data.y,
			Width: data.width,
			Height: data.height
		};

		if (this.sproutFileMultiple === true) {
			// this.sproutFilesChange.emit(this.sproutFiles);
		} else {
			this.sproutTriggerUpload.emit({
				file: this.sproutFile,
				crop: this.sproutFileCrop
			});
		}
	}

	// _

	public _loadImage() {
		this.sproutFile = this.inputSelector.nativeElement.files[0];

		ImageFindDimension(this.sproutFile).then((dimension: IValidatorImage) => {

            this.sproutFileRules = <IValidatorImageRules>{};

			this.sproutFileRules.Width = dimension.Width;
			this.sproutFileRules.Height = dimension.Height;

			let fileSize = this.sproutFile.size / 1024; // kb

			if (ImageValidateDimension(this.sproutFileRules) && (fileSize <= (5 * 1024))) {
				let reader = new FileReader();

				reader.onload = (e: any) => {
					this.editorImage.nativeElement.src = e.target.result;
					this._loadEditor();
				};

				reader.readAsDataURL(this.sproutFile);
			} else {
				console.log('_loadImage : fail');
			}
		}).catch((error) => {
			console.log(error);
		});
	}

	public _loadEditor() {
		let image = this.editorImage.nativeElement as HTMLImageElement;
		let options = {
			aspectRatio: this.sproutFileRules.Ratio,
			viewMode: 2,
			responsive: false,
			movable: false,
			zoomable: false,
			strict: false,
			guides: true,
			highlight: true,
			dragCrop: false,
			cropBoxMovable: true,
			cropBoxResizable: true,
			minContainerWidth: this.sproutFileRules.MinWidth,
			minContainerHeight: this.sproutFileRules.MinHeight,
			minCropBoxWidth: this.sproutFileRules.MinWidth,
			minCropBoxHeight: this.sproutFileRules.MinHeight,
			// crop(event) {
			// 	console.log(event.detail.x);
			// 	console.log(event.detail.y);
			// 	console.log(event.detail.width);
			// 	console.log(event.detail.height);
			// 	console.log(event.detail.rotate);
			// 	console.log(event.detail.scaleX);
			// 	console.log(event.detail.scaleY);
			// },
			// built(event) {
			// 	event.cropper('setCropBoxData', { width: '50', height: '50' });
			// },
		};

		if (typeof this.cropper !== 'undefined') {
			this.cropper.clear();
			this.cropper.destroy();
		}

		this.cropper = new Cropper(image, options);
	}

}
