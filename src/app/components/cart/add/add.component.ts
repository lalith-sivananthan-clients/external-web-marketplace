import { Component, ViewEncapsulation, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IImageRef, IResponseSearch, IResultSearch } from '@app/models/_shared/shared.interfaces';
import { ISearchProduct, ISearchShop, ISearchShopLinks, ISearchProductInventory } from '@app/models/api/search/search.interfaces';

import { LanguageService } from '@app/services/core/language/language.classes';

import { AccountModel } from '@app/models/local/account/account.classes';
import { CartAPI } from '@app/models/api/cart/cart.classes';

import { FormField, FormGroup } from '@app/services/core/validator/form.classes';

@Component({
	selector: 'sprout-cart-add',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './add.component.html',
	styleUrls: [
		'./add.component.css'
	]
})
export class CartAddComponent {

    // binding

    public shop: ISearchShop;
    public product: ISearchProduct;

    @Output()
    sproutShopChange: EventEmitter<ISearchShop> = new EventEmitter<ISearchShop>();

    @Input()
    get sproutShop(): ISearchShop {
        return this.shop;
    }

    set sproutShop(data: ISearchShop) {
        this.shop = data;
        this.sproutShopChange.next(this.shop);

        if (typeof data.ID === 'string' && String(data.ID).length > 0) {
            this.isShopSet = true;
        }
    }

    @Output()
    sproutProductChange: EventEmitter<ISearchProduct> = new EventEmitter<ISearchProduct>();

    @Input()
    get sproutProduct(): ISearchProduct {
        return this.product;
    }

    set sproutProduct(data: ISearchProduct) {
        this.product = data;
        this.sproutProductChange.next(this.product);

        if (typeof data.ID === 'string' && String(data.ID).length > 0) {
            this.isProductSet = true;

            // this._formInit();
        }
    }

    // options

    public isLoading: boolean;
    public isError: boolean;
    public isReady: boolean;
    public isActive: boolean;
    public isInit: boolean;
    public isShopSet: boolean;
    public isProductSet: boolean;

	// form

	public form: FormGroup;
	public inputCount: FormField;

	// message

	public messages: IMessages;
	public message: IMessage;

	//

	// _ events

	constructor(
		public activatedRoute: ActivatedRoute,
		public accountModel: AccountModel,
		public cartAPI: CartAPI
	) {
        this._init();
    }

    //

    add() {
	    if (! this.isShopSet && ! this.isProductSet) {
	        return;
        }

        console.log(this.shop.Name);
        console.log(this.product.Name);

        this.cartAPI.PostCartItemAdd({
            ShopID: this.shop.ID,
            ProductID: this.product.ID,
            Count: 1,
        }).subscribe(
            (response: IResponseSearch) => {
                let _result: IResultSearch = response.Result;

                if (response.Success === true && _result.Code === 1) {
                    console.log('added');
                } else {
                    console.log('error');
                }
            }, error => {
                console.log('error');
            }
        );
    }

	// func : _

	private _init() {
        this.isLoading = false;
        this.isError = false;
        this.isReady = false;
        this.isActive = false;
        this.isShopSet = false;
        this.isProductSet = false;

		this.messages = LanguageService.GetMessages('en');
		this.message = <IMessage>{};

		this.shop = <ISearchShop>{
		    Logo: <IImageRef>{},
            Cover: <IImageRef>{},
            Links: <ISearchShopLinks>{}
        };

        this.product = <ISearchProduct>{
            Logo: <IImageRef>{},
            Cover: <IImageRef>{},
            Marketplace: <ISearchProductInventory>{}
        };
	}

}
