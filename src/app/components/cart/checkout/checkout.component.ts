declare let _: any;

import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IResponseSearch, IResultSearch } from '@app/models/_shared/shared.interfaces';
import { ICartItem, ICartShop } from '@app/models/api/cart/cart.interfaces';

import { LanguageService } from '@app/services/core/language/language.classes';

import { AccountModel } from '@app/models/local/account/account.classes';

import { CartAPI } from '@app/models/api/cart/cart.classes';

@Component({
	selector: 'sprout-cart-single',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './checkout.component.html',
	styleUrls: [
		'./checkout.component.css'
	]
})
export class CartCheckoutComponent {

	// options

    public isLoading: boolean;
    public isError: boolean;
    public isReady: boolean;
    public isActive: boolean;

	// form

	// message

	public messages: IMessages;
	public message: IMessage;

	//

	public items: ICartItem[];
	public shops: ICartShop[];

	// _ events

	constructor(
		public activatedRoute: ActivatedRoute,
		public accountModel: AccountModel,
		public cartAPI: CartAPI
	) {
	    this._init();
        this._getCart();
	}

    // func

    public round(data: number) {
	    return _.round(data, 2);
    }

    public confirm() {
        this.cartAPI.PostCartCheckout({}).subscribe(
            (response: IResponseSearch) => {
                let _result: IResultSearch = response.Result;

                if (response.Success === true && _result.Code === 1) {
                    this._init();
                    this._getCart();
                } else {
                }
            },
            error => {
            }
        );
    }

	// func

	private _getCart() {
		this.cartAPI.PostCartGet({}).subscribe(
			(response: IResponseSearch) => {
				let _result: IResultSearch = response.Result;

				if (response.Success === true && _result.Code === 1) {
				    this.items = _result.Data || [];

				    this._processItems();
				} else {
				}
			},
			error => {
			}
		);
	}

    private _processItems() {
	    if (this.items.length <= 0) {
	        return;
        }

        for (let iItems = 0; iItems < this.items.length; iItems++) {
            let _shopExist = false;
            let _shop: ICartShop;

            if (this.shops.length > 0) {
                for (let iShop = 0; iShop < this.shops.length; iShop++) {
                    if (this.items[iItems].ShopID === this.shops[iShop].ShopID) {
                        _shopExist = true;
                    }
                }

                if (_shopExist === false) {
                    _shop = {
                        ShopID: this.items[iItems].ShopID,
                        Shop: this.items[iItems].Shop,
                        Items: [],
                        TotalPrice: 0
                    };

                    this.shops.push(_shop);
                }
            } else {
                _shop = {
                    ShopID: this.items[iItems].ShopID,
                    Shop: this.items[iItems].Shop,
                    Items: [],
                    TotalPrice: 1
                };

                this.shops.push(_shop);
            }

            for (let iShop = 0; iShop < this.shops.length; iShop++) {
                if (this.items[iItems].ShopID === this.shops[iShop].ShopID) {
                    this.shops[iShop].Items.push(this.items[iItems]);
                    this.shops[iShop].TotalPrice = (this.shops[iShop].TotalPrice * this.items[iItems].Count) + this.items[iItems].Product.Marketplace.Price;
                }
            }
        }
    }

	// func : _

	private _init() {
        this.isLoading = false;
        this.isError = false;
        this.isReady = false;
        this.isActive = false;

		this.messages = LanguageService.GetMessages('en');
		this.message = <IMessage>{};

		this.items = [];
		this.shops = [];
	}

}
