declare let _: any;

import { Component, ViewEncapsulation, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IResponseSearch, IResultSearch } from '@app/models/_shared/shared.interfaces';
import { ICartItem } from '@app/models/api/cart/cart.interfaces';
import { IShopSimple } from '@app/models/api/shop/shop.interfaces';
import { IProductSimple } from '@app/models/api/shop-product/product.interfaces';

import { ValidatorRuleEnum } from '@app/services/core/validator/validator.enums';

import { LanguageService } from '@app/services/core/language/language.classes';

import { AccountModel } from '@app/models/local/account/account.classes';

import { CartAPI } from '@app/models/api/cart/cart.classes';

import { FormField, FormGroup } from '@app/services/core/validator/form.classes';


@Component({
	selector: 'sprout-cart-single-product',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './product.component.html',
	styleUrls: [
		'./product.component.css'
	]
})
export class CartSingleProductComponent {

    // binding

    public cart: ICartItem;

    @Output()
    sproutCartChange: EventEmitter<ICartItem> = new EventEmitter<ICartItem>();

    @Input()
    get sproutCart(): ICartItem {
        return this.cart;
    }

    set sproutCart(data: ICartItem) {
        this.cart = data;
        this.sproutCartChange.next(this.cart);

        if (typeof data.ProductID === 'string' && String(data.ProductID).length > 0) {
            this.isCartSet = true;

            this._formInit();
        }
    }

    // options

    public isLoading: boolean;
    public isError: boolean;
    public isReady: boolean;
    public isActive: boolean;
    public isInit: boolean;
    public isCartSet: boolean;

	// form

	public form: FormGroup;
	public inputQuantity: FormField;

	// message

	public messages: IMessages;
	public message: IMessage;

	//

	// _ events

	constructor(
		public activatedRoute: ActivatedRoute,
		public accountModel: AccountModel,
		public cartAPI: CartAPI
	) {
        this._init();
        this._formInit();
    }

    //

    round(data: number) {
	    return _.round(data, 2);
    }

    //

    updateQuantity() {
	    if (! this.isCartSet) {
	        return;
        }

        this.cartAPI.PostCartItemUpdate({
            ProductID: this.cart.ProductID,
            Count: this.inputQuantity.Value
        }).subscribe(
            (response: IResponseSearch) => {
                let _result: IResultSearch = response.Result;

                if (response.Success === true && _result.Code === 1) {
                    this.cart.Count = Number(this.inputQuantity.Value);

                    this.message = this.messages.GlobalBasic.Success;
                    this.message.Show = true;
                } else {
                    this.message = this.messages.GlobalBasic.Error;
                    this.message.Show = true;
                }
            }, error => {
                this.message = this.messages.GlobalBasic.Server;
                this.message.Show = true;
            }
        );
    }

    remove() {
        if (!this.isCartSet) {
            return;
        }

        this.cartAPI.PostCartItemRemove({
            ProductID: this.cart.ProductID,
        }).subscribe(
            (response: IResponseSearch) => {
                let _result: IResultSearch = response.Result;

                if (response.Success === true && _result.Code === 1) {
                    this.isActive = false;
                } else {
                    console.log('error');
                }
            }, error => {
                console.log('error');
            }
        );
    }

	// func : _

	private _init() {
        this.isLoading = false;
        this.isError = false;
        this.isReady = false;
        this.isActive = true;
        this.isCartSet = false;

		this.messages = LanguageService.GetMessages('en');
		this.message = <IMessage>{};

		this.cart = <ICartItem>{
            Shop: <IShopSimple>{},
            Product: <IProductSimple>{}
        };
	}

    private _formInit() {
        this.form = new FormGroup();

        this.inputQuantity = new FormField('inputQuantity', 'Quantity', this.cart.Count, [
            { Name: ValidatorRuleEnum.Required, Options: [] },
            { Name: ValidatorRuleEnum.Number, Options: [] }
        ]);

        this.form.SetFields([ this.inputQuantity ]);
        this.form.SetParent();
    }

}
