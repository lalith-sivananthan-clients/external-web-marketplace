declare let _: any;

import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IResponseSearch, IResultSearch } from '@app/models/_shared/shared.interfaces';
import { ICartItem } from '@app/models/api/cart/cart.interfaces';

import { LanguageService } from '@app/services/core/language/language.classes';

import { AccountModel } from '@app/models/local/account/account.classes';
import { CartAPI } from '@app/models/api/cart/cart.classes';

@Component({
	selector: 'sprout-cart-single',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './single.component.html',
	styleUrls: [
		'./single.component.css'
	]
})
export class CartSingleComponent implements OnInit {

	// options

    public isLoading: boolean;
    public isError: boolean;
    public isReady: boolean;
    public isActive: boolean;

	// form

	// message

	public messages: IMessages;
	public message: IMessage;

	//

	public carts: ICartItem[];

	// _ events

	constructor(
		public activatedRoute: ActivatedRoute,
		public accountModel: AccountModel,
		public cartAPI: CartAPI
	) {
	}

	ngOnInit() {
        this._init();
        this._getCart();
    }

	public getTotal(): number {
	    let _total = 0;

        if (this.carts !== null && this.carts.length > 0) {
            this.carts.forEach((cart) => {
                _total = (_total) + ((cart.Product.Marketplace.Price / 100) * cart.Count);
            });
        }

	    return _.round(_total, 2);
    }

	// func

    public isCartEmpty() {
	    if (this.carts === null) {
	        return false;
        }

	    return this.carts.length > 0;
    }

    // func : _

	private _getCart() {
		this.cartAPI.PostCartGet({}).subscribe(
			(response: IResponseSearch) => {
				let _result: IResultSearch = response.Result;

				if (response.Success === true && _result.Code === 1) {
				    this.carts = _result.Data;
				} else {
				}
			}, () => {
			}
		);
	}

	// func : _

	private _init() {
        this.isLoading = false;
        this.isError = false;
        this.isReady = false;
        this.isActive = false;

		this.messages = LanguageService.GetMessages('en');
		this.message = <IMessage>{};

		this.carts = [];
	}

}
