import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'sprout-intro',
    encapsulation: ViewEncapsulation.Emulated,
    templateUrl: './intro.component.html',
    styleUrls: [
        './intro.component.css'
    ]
})
export class IntroComponent {

    constructor() {}

}
