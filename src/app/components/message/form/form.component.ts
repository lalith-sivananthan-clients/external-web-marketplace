import { Component, ViewEncapsulation, EventEmitter, Input, Output, DoCheck, OnInit, OnDestroy } from '@angular/core';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';

import { LanguageService } from '@app/services/core/language/language.classes';
import { MessagerService } from '@app/services/core/messenger/messenger.classes';

@Component({
	selector: 'sprout-message-form',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './form.component.html',
	styleUrls: [
		'./form.component.css'
	]
})
export class MessageFormComponent implements OnInit, OnDestroy, DoCheck {

	// binding
	public messages: IMessages = LanguageService.GetMessages('en');
	public message: IMessage = <IMessage>{};

	@Output()
	sproutMessageChange: EventEmitter<IMessage> = new EventEmitter<IMessage>();

	@Input()
	get sproutMessage(): IMessage {
		return this.message;
	}

	set sproutMessage(data: IMessage) {
		this.message = data;
		this.sproutMessageChange.next(this.message);
	}

	//

	private messageTimeout: any = null;

	constructor(
		public accountModel: AccountModel,
		public messagerService: MessagerService
	) {
		this.message.Show = false;
	}

	ngOnInit() {
	}

	ngOnDestroy() {
	}

	ngDoCheck() {
		if (this.sproutMessage.Show === true && this.messageTimeout === null) {
			this.messageTimeout = setTimeout(() => {
				this.messageTimeout = null;
				this.sproutMessage = <IMessage>{};
			}, 4000);
		}
	}

	// func : toggle

	public toggle() {
		this.message.Show = ! this.message.Show;
	}

}
