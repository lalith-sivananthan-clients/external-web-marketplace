import { Subscription } from 'rxjs';

import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';
import { AccountAPI } from '@app/models/api/account/account.classes';

import { LanguageService } from '@app/services/core/language/language.classes';
import { MessagerService } from '@app/services/core/messenger/messenger.classes';

import { MessengerGlobalEvent } from '@app/services/core/messenger/messenger.variables';

@Component({
    selector: 'sprout-message-global',
    encapsulation: ViewEncapsulation.Emulated,
    templateUrl: './global.component.html',
    styleUrls: [
        './global.component.css'
    ]
})
export class MessageGlobalComponent implements OnInit, OnDestroy {

    // message

    public messages: IMessages = LanguageService.GetMessages('en');
    public message: IMessage = <IMessage>{};

    //

    public showEmailConfirmCheck: boolean;

    // _ events

    private _subscribeMessengerGlobal: Subscription;

    constructor(
        public accountAPI: AccountAPI,
        public accountModel: AccountModel,
        public messagerService: MessagerService
    ) {
        this.showEmailConfirmCheck = false;
        this.message.Show = false;
    }

    ngOnInit() {
        this._subscribeMessengerGlobal = MessengerGlobalEvent.subscribe((data) => {
            this.message = data;

            if (typeof data.Name === 'undefined') {
                return;
            }

            this.message.Show = true;
            this.showEmailConfirmCheck = this.message.Name === 'global-email-confirm-check';
        });

        this.messagerService.CheckGlobal();
    }

    ngOnDestroy() {
        this._subscribeMessengerGlobal.unsubscribe();
    }

    // func

    emailConfirmSend() {
        if (this.accountModel.AccountConfirmed === false) {
            this.accountAPI.PostAccountConfirmResend({}).subscribe(
                (response: IResponse) => {
                    let _result: IResult = response.Result;

                    if (response.Success === true && _result.Code === 1) {
                        MessengerGlobalEvent.next(this.messages.GlobalEmail.ConfirmSendSuccessful);
                    } else {
                        MessengerGlobalEvent.next(this.messages.GlobalEmail.ConfirmSendError);
                    }
                },
                error => {
                    MessengerGlobalEvent.next(this.messages.GlobalEmail.ConfirmSendError);
                }
            );
        } else {
            MessengerGlobalEvent.next(this.messages.GlobalEmail.ConfirmSendError);
        }
    }

    // func : toggle

    public toggle() {
        this.message.Show = !this.message.Show;
    }

}
