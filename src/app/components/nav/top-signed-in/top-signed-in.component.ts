import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { ImageRefStatusEnum } from '@app/models/_shared/shared.enums';

import { AccountModel } from '@app/models/local/account/account.classes';

import { URLConfig } from '@app/configs/config.consts';

@Component({
    selector: 'sprout-nav-top-signed-in',
    encapsulation: ViewEncapsulation.Emulated,
    templateUrl: './top-signed-in.component.html',
    styleUrls: [
        './top-signed-in.component.css'
    ]
})
export class NavTopSignedInComponent {

    //

    public isVisible: boolean;

    public url: any;

    constructor(
        public router: Router,
        public accountModel: AccountModel
    ) {
        this.isVisible = false;
        this.url = URLConfig;
    }

    // func

    public imageStatus(data: ImageRefStatusEnum) {
        if (data === ImageRefStatusEnum.NotSet && this.accountModel.AccountAvatarStatus === ImageRefStatusEnum.NotSet) {
            return true;
        }

        if (data === ImageRefStatusEnum.Set && this.accountModel.AccountAvatarStatus === ImageRefStatusEnum.Set) {
            return true;
        }

        if (data === ImageRefStatusEnum.Processing && this.accountModel.AccountAvatarStatus === ImageRefStatusEnum.Processing) {
            return true;
        }

        return false;
    }

    public menu() {
        this.isVisible = ! this.isVisible;
    }

    public logout() {
        this.accountModel.Logout();
        document.location.href = URLConfig.WebAccount;
    }

}
