import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IImageRef, IResponseSearch, IResultSearch } from '@app/models/_shared/shared.interfaces';
import { ISearchProduct, ISearchShop, ISearchShopLinks, ISearchProductInventory } from '@app/models/api/search/search.interfaces';

import { ImageRefStatusEnum } from '@app/models/_shared/shared.enums';

import { LanguageService } from '@app/services/core/language/language.classes';

import { AccountModel } from '@app/models/local/account/account.classes';
import { SearchAPI } from '@app/models/api/search/search.classes';

import { FormField, FormGroup } from '@app/services/core/validator/form.classes';

import { URLConfig } from '@app/configs/config.consts';

import { ProductTaxonomyService } from '@app/services/core/helpers/product-taxonomy.classes';

@Component({
	selector: 'sprout-product-single',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './single.component.html',
	styleUrls: [
		'./single.component.css'
	]
})
export class ProductSingleComponent implements OnInit, OnDestroy {

    // options

    public isLoading: boolean;
    public isError: boolean;
    public isReady: boolean;
    public isActive: boolean;

	// form

	public form: FormGroup;
	public inputQuery: FormField;

	// message

    public messages: IMessages;
    public message: IMessage;

	//
    public shopID: string;
    public productID: string;

    public shop: ISearchShop;
    public product: ISearchProduct;

	public products: ISearchProduct[];

	// _ events

	private _subscribeParams: any;

	constructor(
		public activatedRoute: ActivatedRoute,
		public accountModel: AccountModel,
		public searchAPI: SearchAPI,
        public productService: ProductTaxonomyService
	) {
	}

	ngOnInit() {
		this._init();

		this._subscribeParams = this.activatedRoute.params.subscribe(params => {
			this.productID = params['productID'];
			this._getProduct();
		});
	}

	ngOnDestroy() {
		this._subscribeParams.unsubscribe();
	}

	// func

	getLogo() {
		if (this.product.Logo.Status === ImageRefStatusEnum.Set) {
			return URLConfig.GatewayMedia + 'images/' + this.product.Logo.MediaID + '_500_500.jpg';
		}

		return 'assets/images/default-product.png';
	}

	getShopLogo() {
		if (this.shop.Logo.Status === ImageRefStatusEnum.Set) {
			return URLConfig.GatewayMedia + 'images/' + this.shop.Logo.MediaID + '_500_500.jpg';
		}

		return 'assets/images/default-shop.png';
	}

    getShopCover() {
        if (this.shop.Cover.Status === ImageRefStatusEnum.Set) {
            return URLConfig.GatewayMedia + 'images/' + this.shop.Cover.MediaID + '_700_300.jpg';
        }

        return 'assets/images/default-shop-cover.png';
    }

	getProductLogo(product: ISearchProduct) {
		if (product.Logo.Status === ImageRefStatusEnum.Set) {
			return URLConfig.GatewayMedia + 'images/' + product.Logo.MediaID + '_500_500.jpg';
		}

		return 'assets/images/default-product.png';
	}

	getProductCover() {
		if (this.product.Cover.Status === ImageRefStatusEnum.Set) {
			return URLConfig.GatewayMedia + 'images/' + this.product.Cover.MediaID + '_700_300.jpg';
		}

		return 'assets/images/default-shop-cover.png';
	}

	// func : _

	public _getProduct() {
		this.searchAPI.PostProduct({
			ID: this.productID
		}).subscribe(
			(response: IResponseSearch) => {
				let _result: IResultSearch = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this.product = _result.Data;
					this.shopID = this.product.ShopID;

					this._getProducts();
					this._getShop();
				} else {
				}
			},
			error => {
			}
		);
	}

	public _getProducts() {
		this.searchAPI.PostProducts({
			Query: '',
			Page: 1,
			Limit: 3
		}).subscribe(
			(response: IResponseSearch) => {
				let _result: IResultSearch = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this.products = _result.Data;
				} else {
				}
			},
			error => {
			}
		);
	}

	public _getShop() {
		this.searchAPI.PostShop({
			ID: this.shopID
		}).subscribe(
			(response: IResponseSearch) => {
				let _result: IResultSearch = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this.shop = _result.Data;
				} else {
				}
			},
			error => {
			}
		);
	}

	// func : _

	private _init() {
        this.isLoading = false;
        this.isError = false;
        this.isReady = false;
        this.isActive = false;

        this.messages = LanguageService.GetMessages('en');
        this.message = <IMessage>{};

        this.shopID = this.shopID || null;
        this.productID = this.productID || null;

        this.shop = <ISearchShop>{
            Logo: <IImageRef>{},
            Cover: <IImageRef>{},
            Links: <ISearchShopLinks>{}
        };

		this.product = <ISearchProduct>{
            Logo: <IImageRef>{},
            Cover: <IImageRef>{},
            Marketplace: <ISearchProductInventory>{}
        };

		this.products = [];
	}

}
