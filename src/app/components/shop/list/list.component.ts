import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { ISearchShop } from '@app/models/api/search/search.interfaces';
import { ImageRefStatusEnum } from '@app/models/_shared/shared.enums';
import { IResponseSearch, IResultSearch } from '@app/models/_shared/shared.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';
import { SearchAPI } from '@app/models/api/search/search.classes';

import { LanguageService } from '@app/services/core/language/language.classes';
import { PaginationService } from '@app/services/core/pagination/pagination.classes';

import { FormField, FormGroup } from '@app/services/core/validator/form.classes';

import { URLConfig } from '@app/configs/config.consts';

@Component({
	selector: 'sprout-shop-list',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './list.component.html',
	styleUrls: [
		'./list.component.css'
	]
})
export class ShopListComponent implements OnInit, OnDestroy {

	// options

    public isLoading: boolean;
    public isError: boolean;
    public isReady: boolean;
    public isActive: boolean;

	// form

	public form: FormGroup;
	public inputQuery: FormField;

	// message

	public messages: IMessages;
	public message: IMessage;

	//

    public pageCurrent: number;
    public pageTotal: number;

    public shops: ISearchShop[];

	// _ events

	private _subscribeParams: any;

	constructor(
		public activatedRoute: ActivatedRoute,
        public pagination: PaginationService,
		public accountModel: AccountModel,
		public searchAPI: SearchAPI
	) {
	}

	ngOnInit() {
		this._init();
		this._formInit();

		this.submit();

		this._subscribeParams = this.activatedRoute.params.subscribe(params => {
			let _page = parseInt(params['page'], 10);

			if (! isNaN(_page)) {
				this.pageCurrent = _page;
				this.submit();
			}
		});
	}

	ngOnDestroy() {
		this._subscribeParams.unsubscribe();
	}

    // func

	getLogo(shop: ISearchShop) {
		if (shop.Logo.Status === ImageRefStatusEnum.Set) {
			return URLConfig.GatewayMedia + 'images/' + shop.Logo.MediaID + '_500_500.jpg';
		}

		return 'assets/images/default-shop.png';
	}

	// func

	public submit() {
		this.searchAPI.PostShops({
			Query: this.inputQuery.Value,
			Page: this.pageCurrent,
			Limit: 4
		}).subscribe(
			(response: IResponseSearch) => {
				let _result: IResultSearch = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this.shops = _result.Data;

					this.pageCurrent = _result.PageCurrent;
					this.pageTotal = _result.PageTotal;
                    this.pagination.Set(this.pageCurrent, this.pageTotal);
				} else {
				}
			},
			error => {
			}
		);
	}

	// func : _

	private _init() {
        this.isLoading = false;
        this.isError = false;
        this.isReady = false;
        this.isActive = false;

		this.messages = LanguageService.GetMessages('en');
		this.message = <IMessage>{};

        this.pageCurrent = this.pageCurrent || 1;
        this.pageTotal = this.pageTotal || 1;
        this.pagination.Set(this.pageCurrent, this.pageTotal);

		this.shops = [];
	}

	private _formInit() {
		this.form = new FormGroup();

		this.inputQuery = new FormField('search', 'Search', '', [
		]);

		this.form.SetFields([ this.inputQuery ]);
		this.form.SetParent();
	}

}
