import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { ImageRefStatusEnum } from '@app/models/_shared/shared.enums';
import {IImageRef, IResponseSearch, IResultSearch} from '@app/models/_shared/shared.interfaces';
import { ISearchOwner, ISearchOwnerProfile, ISearchProduct, ISearchShop, ISearchShopLinks } from '@app/models/api/search/search.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';
import { SearchAPI } from '@app/models/api/search/search.classes';

import { LanguageService } from '@app/services/core/language/language.classes';
import { PaginationService } from '@app/services/core/pagination/pagination.classes';

import { FormField, FormGroup } from '@app/services/core/validator/form.classes';

import { URLConfig } from '@app/configs/config.consts';

@Component({
	selector: 'sprout-shop-single-product-list',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './list.component.html',
	styleUrls: [
		'./list.component.css'
	]
})
export class ShopSingleProductListComponent implements OnInit, OnDestroy {

    // options

    public isLoading: boolean;
    public isError: boolean;
    public isReady: boolean;
    public isActive: boolean;

    // form

    public form: FormGroup;
    public inputQuery: FormField;

    // message

    public messages: IMessages;
    public message: IMessage;

    //

    public pageCurrent: number;
    public pageTotal: number;

    public shopID: string;
    public shop: ISearchShop;
    public owner: ISearchOwner;

	public products: ISearchProduct[];

	// _ events

	private _subscribeParams: any;

	constructor(
        public activatedRoute: ActivatedRoute,
        public pagination: PaginationService,
        public accountModel: AccountModel,
        public searchAPI: SearchAPI
	) {
	}

	ngOnInit() {
		this._init();
		this._formInit();

		this._subscribeParams = this.activatedRoute.params.subscribe(params => {
            this.shopID = params['storeID'];

            let _page = parseInt(params['page'], 10);

            if (! isNaN(_page)) {
                this.pageCurrent = _page;
                this.submit();
            }

            this._getShop();
		});
	}

	ngOnDestroy() {
		this._subscribeParams.unsubscribe();
	}

	// func

    getShopLogo() {
        if (this.shop.Logo.Status === ImageRefStatusEnum.Set) {
            return URLConfig.GatewayMedia + 'images/' + this.shop.Logo.MediaID + '_500_500.jpg';
        }

        return 'assets/images/default-shop.png';
    }

    getShopCover() {
        if (this.shop.Cover.Status === ImageRefStatusEnum.Set) {
            return URLConfig.GatewayMedia + 'images/' + this.shop.Cover.MediaID + '_700_300.jpg';
        }

        return 'assets/images/default-shop-cover.png';
    }

    getOwnerLogo() {
        if (this.owner.Profile.Avatar.Status === ImageRefStatusEnum.Set) {
            return URLConfig.GatewayMedia + 'images/' + this.owner.Profile.Avatar.MediaID + '_500_500.jpg';
        }

        return 'assets/images/default-account.png';
    }

	getLogo(product: ISearchShop) {
		if (product.Logo.Status === ImageRefStatusEnum.Set) {
			return URLConfig.GatewayMedia + 'images/' + product.Logo.MediaID + '_500_500.jpg';
		}

		return 'assets/images/default-product.png';
	}

	public submit() {
		this.searchAPI.PostProductsForShop({
			ID: this.shopID,
			Page: this.pageCurrent,
			Limit: 3
		}).subscribe(
			(response: IResponseSearch) => {
				let _result: IResultSearch = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this.products = _result.Data;

                    this.pageCurrent = _result.PageCurrent;
                    this.pageTotal = _result.PageTotal;
                    this.pagination.Set(this.pageCurrent, this.pageTotal);
				} else {
				}
			},
			error => {
			}
		);
	}

	// func : _

    public _getShop() {
        this.searchAPI.PostShop({
            ID: this.shopID
        }).subscribe(
            (response: IResponseSearch) => {
                let _result: IResultSearch = response.Result;

                if (response.Success === true && _result.Code === 1) {
                    this.shop = _result.Data;

                    this._getOwner();
                } else {
                }
            },
            error => {
            }
        );
    }

    public _getOwner() {
        this.searchAPI.PostOwner({
            ID: this.shop.AccountID
        }).subscribe(
            (response: IResponseSearch) => {
                let _result: IResultSearch = response.Result;

                if (response.Success === true && _result.Code === 1) {
                    this.owner = _result.Data;
                } else {
                }
            },
            error => {
            }
        );
    }

	private _init() {
        this.isLoading = false;
        this.isError = false;
        this.isReady = false;
        this.isActive = false;

        this.messages = LanguageService.GetMessages('en');
        this.message = <IMessage>{};

        this.shopID = this.shopID || null;

        this.shop = <ISearchShop>{
            Logo: <IImageRef>{},
            Cover: <IImageRef>{},
            Links: <ISearchShopLinks>{}
        };

        this.owner = <ISearchOwner>{
            Profile: <ISearchOwnerProfile>{
                Avatar: <IImageRef>{}
            }
        };

        this.shopID = this.shopID || null;
        this.pageCurrent = this.pageCurrent || 1;
        this.pageTotal = this.pageTotal || 1;
        this.pagination.Set(this.pageCurrent, this.pageTotal);

        this.products = [];
	}

	private _formInit() {
		this.form = new FormGroup();

		this.inputQuery = new FormField('search', 'Search', '', [
		]);

		this.form.SetFields([ this.inputQuery ]);
		this.form.SetParent();
	}

}
