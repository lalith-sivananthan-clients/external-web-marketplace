// export const StorageConfig = {
//     'Name': 'localhost',
//     'Path': '/',
//     'Domain': '127.0.0.1'
// };
//
// export const URLConfig = {
//     'APIAuthentication': 'http://127.0.0.1:9000/v1/',
//     'APIConnect':        'http://127.0.0.1:9001/v1/',
//     'APIAccount':        'http://127.0.0.1:9002/v1/',
//     'APIMedia':          'http://127.0.0.1:9003/v1/',
//     'APIShop':           'http://127.0.0.1:9004/v1/',
//     'APISearch':         'http://127.0.0.1:9005/v1/',
//     'APITransaction':    'http://127.0.0.1:9006/v1/',
//     'APIInbox':          'http://127.0.0.1:9007/v1/',
//     'APISocialize':      'http://127.0.0.1:9008/v1/',
//
//     'GatewayConnect': 'http://127.0.0.1:8000/',
//     'GatewayMedia':   'http://127.0.0.1:8001/',
//
//     'WebHome':        'https://127.0.0.1:7000/',
//     'WebAccount':     'https://127.0.0.1:7001/',
//     'WebManage':      'https://127.0.0.1:7002/',
//     'WebMarketplace': 'https://127.0.0.1:7003/',
//     'WebBrokerage':   'https://127.0.0.1:7004/'
// };

export const StorageConfig = {
    'Name': 'sproutways',
    'Path': '/',
    'Domain': '.sproutways.com'
};

export const URLConfig = {
    'APIAuthentication': 'http://authentication.api.sproutways.com/v1/',
    'APIConnect':        'http://connect.api.sproutways.com/v1/',
    'APIAccount':        'http://account.api.sproutways.com/v1/',
    'APIMedia':          'http://media.api.sproutways.com/v1/',
    'APIShop':           'http://shop.api.sproutways.com/v1/',
    'APISearch':         'http://search.api.sproutways.com/v1/',
    'APITransaction':    'http://transaction.api.sproutways.com/v1/',
    'APIInbox':          'http://inbox.api.sproutways.com/v1/',
    'APISocialize':      'http://socialize.api.sproutways.com/v1/',

    'GatewayConnect': 'https://connect.gateway.sproutways.com/',
    'GatewayMedia':   'https://media.gateway.sproutways.com/',

    'WebHome':        'https://sproutways.com/',
    'WebAccount':     'https://account.sproutways.com/',
    'WebManage':      'https://manage.sproutways.com/',
    'WebMarketplace': 'https://marketplace.sproutways.com/',
    'WebBrokerage':   'https://brokerage.sproutways.com/'
};
