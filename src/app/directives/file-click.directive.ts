import { Directive, EventEmitter, ElementRef, Input, HostListener, Output } from '@angular/core';

@Directive({
	selector: '[sproutFileClick]'
})
export class FileClickDirective {

	// input

	@Input()
	sproutFileClick: File;

	// output

	@Output()
	sproutFileClickChange: EventEmitter<File> = new EventEmitter<File>();

	//

	protected element: ElementRef;

	public constructor(elementRef: ElementRef) {
		this.element = elementRef;
	}

	@HostListener('click')
	click() {
	}

}
