import { Directive, ElementRef } from '@angular/core';

@Directive({
	selector: '[sproutFileDrop][routerLink]'
})
export class FileDropDirective {

	protected element: ElementRef;

	public constructor(element: ElementRef) {
		this.element = element;
	}

}
