declare let $: any;

import { Directive, Input, HostListener } from '@angular/core';

@Directive({
	selector: '[sproutFocus]'
})
export class FocusDirective {

	@Input()
	sproutFocus: string;

	constructor() {
	}

	@HostListener('click', ['$event'])
	click() {
		$(document).ready(() => {
			$('#' + this.sproutFocus).focus();

			setTimeout(() => {
				$('#' + this.sproutFocus).focus();
			}, 50);
		});
	}

}
