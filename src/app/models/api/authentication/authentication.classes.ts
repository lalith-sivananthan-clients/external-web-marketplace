declare let $: any;

import { Observable } from 'rxjs';

import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { IResponse } from '@app/models/_shared/shared.interfaces';

import { URLConfig } from '@app/configs/config.consts';

import { GetHeader } from '@app/models/_shared/shared.functions';

@Injectable()
export class AuthenticationAPI {

	constructor(private http: HttpClient) { }

	public PostCreateAccount(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIAuthentication + 'create-account', $.param(body), GetHeader());
	}

	public PostSignIn(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIAuthentication + 'sign-in', $.param(body), GetHeader());
	}

	public PostPasswordForgot(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIAuthentication + 'password/forgot', $.param(body), GetHeader());
	}

	public PostPasswordChange(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIAuthentication + 'password/change', $.param(body), GetHeader());
	}

	async PostUsernameUnique(body: Object) {
		try {
			return await this.http.post(URLConfig.APIAuthentication + 'username/unique', $.param(body), GetHeader()).toPromise();
		} catch (error) {
			return error;
		}
	}

	async PostEmailUnique(body: Object) {
		try {
			return await this.http.post(URLConfig.APIAuthentication + 'email/unique', $.param(body), GetHeader()).toPromise();
		} catch (error) {
			return error;
		}
	}

}
