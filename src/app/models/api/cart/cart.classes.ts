declare let $: any;

import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { IResponse } from '@app/models/_shared/shared.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';

import { GetHeaderWithUser } from '@app/models/_shared/shared.functions';

import { URLConfig } from '@app/configs/config.consts';

@Injectable()
export class CartAPI {

	constructor(
		private http: HttpClient,
		private accountModel: AccountModel
	) { }

    // cart

    public PostCartGet(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APITransaction + 'cart/get', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostCartCheckout(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APITransaction + 'cart/checkout', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostCartClear(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APITransaction + 'cart/clear', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

	// cart -> item

	public PostCartItemAdd(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APITransaction + 'cart/item/add', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostCartItemUpdate(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APITransaction + 'cart/item/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostCartItemRemove(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APITransaction + 'cart/item/remove', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

}
