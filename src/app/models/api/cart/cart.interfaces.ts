import { IShopSimple } from '@app/models/api/shop/shop.interfaces';
import { IProductSimple } from '@app/models/api/shop-product/product.interfaces';

// cart

export interface ICart {
    ID: string;
    AccountID: string;
    ShopID: string;
    ProductID: string;
    Shop: IShopSimple;
    Product: IProductSimple;
    Count: number;
}

export interface ICartShop {
    ShopID: string;
    Shop: IShopSimple;
    Items: ICartItem[];
    TotalPrice: number;
}

export interface ICartItem {
    ID: string;
    AccountID: string;
    ShopID: string;
    ProductID: string;
    Shop: IShopSimple;
    Product: IProductSimple;
    Count: number;
}
