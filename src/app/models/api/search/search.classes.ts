declare let $: any;

import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { IResponse } from '@app/models/_shared/shared.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';

import { GetHeaderWithUser } from '@app/models/_shared/shared.functions';

import { URLConfig } from '@app/configs/config.consts';

@Injectable()
export class SearchAPI {

	constructor(
		private http: HttpClient,
		private accountModel: AccountModel
	) { }

    // shop

    public PostShops(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APISearch + 'shops', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostShopsForOwner(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APISearch + 'shops/for-account', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostShop(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APISearch + 'shop', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

	// product

	public PostProducts(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APISearch + 'products', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostProductsForShop(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APISearch + 'products/for-shop', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostProduct(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APISearch + 'product', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	// owner

	public PostOwners(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APISearch + 'owners', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostOwner(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APISearch + 'owner', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

}
