import { IImageRef, IWhen } from '@app/models/_shared/shared.interfaces';
import { IShopLinks, IShopSettings } from '@app/models/api/shop/shop.interfaces';

// shop

export interface ISearchShop {
    ID: string;
    AccountID: string;
    Name: string;
    Slug: string;
    Story: string;
    Logo: IImageRef;
    Cover: IImageRef;
    Links: IShopLinks;
}

export interface ISearchShopLinks {
    Website: string;
    Blog: string;
    Facebook: string;
    Instagram: string;
    Twitter: string;
    LinkedIn: string;
}

// product

export interface ISearchProduct {
    ID: string;
    ShopID: string;
    Name: string;
    Slug: string;
    Description: string;
    SKU: string;
    Status: string;
    Group: string;
    Class: string;
    Method: string;
    Type: string;
    THC: number;
    CBD: number;
    Unit: number;
    UnitType: string;
    UnitPrice: number;
    Marketplace: ISearchProductInventory;
    Brokerage: ISearchProductInventory;
    Logo: IImageRef;
    Cover: IImageRef;
}

export interface ISearchProductInventory {
    Status: string;
    Unit: string;
    Price: number;
    Inventory: number;
}

// owner

export interface ISearchOwner {
	ID: string;
	Username: string;
	Profile: ISearchOwnerProfile;
}

export interface ISearchOwnerProfile {
	Avatar: IImageRef;
	NameFirst: string;
	NameLast: string;
	Headline: string;
	Story: string;
}
