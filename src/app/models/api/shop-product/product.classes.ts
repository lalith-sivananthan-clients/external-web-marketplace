declare let $: any;

import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { IResponse } from '@app/models/_shared/shared.interfaces';
import { IImageCrop } from '@app/components/_shared/uploader/uploader.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';

import { GetHeaderWithUser, GetUploadHeaderWithUser } from '@app/models/_shared/shared.functions';

import { URLConfig } from '@app/configs/config.consts';

@Injectable()
export class ShopProductAPI {

	constructor(
		private http: HttpClient,
		private accountModel: AccountModel
	) { }

    public PostList(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'product/list', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostSingle(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'product/single', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

	public PostSingleCreate(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIShop + 'product/single/create', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostSingleUpdate(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIShop + 'product/single/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostSingleDelete(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIShop + 'product/single/delete', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	//

	public PostSingleLogoUpload(file: File, options: IImageCrop, shopID: string, productID: string): Observable<any> {
		const formData: FormData = new FormData();
		formData.append('File', file);
		formData.append('ShopID', shopID);
		formData.append('ProductID', productID);
		formData.append('XOffset', options.XOffset.toString());
		formData.append('YOffset', options.YOffset.toString());
		formData.append('Width', options.Width.toString());
		formData.append('Height', options.Height.toString());

		return this.http.post<IResponse>(URLConfig.APIShop + 'product/single/logo/upload', formData, GetUploadHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostSingleCoverUpload(file: File, options: IImageCrop, shopID: string, productID: string): Observable<any> {
		const formData: FormData = new FormData();
		formData.append('File', file);
		formData.append('ShopID', shopID);
		formData.append('ProductID', productID);
		formData.append('XOffset', options.XOffset.toString());
		formData.append('YOffset', options.YOffset.toString());
		formData.append('Width', options.Width.toString());
		formData.append('Height', options.Height.toString());

		return this.http.post<IResponse>(URLConfig.APIShop + 'product/single/cover/upload', formData, GetUploadHeaderWithUser(this.accountModel.TokenLocalValue));
	}

}
