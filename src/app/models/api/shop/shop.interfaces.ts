import { IImageRef, IWhen } from '@app/models/_shared/shared.interfaces';

export interface IShop {
	ID: string;
	AccountID: string;
	Name: string;
	Slug: string;
	Story: string;
    Logo: IImageRef;
    Cover: IImageRef;
    Links: IShopLinks;
	Settings: IShopSettings;
	When: IWhen;
}

export interface IShopSimple {
    ID: string;
    AccountID: string;
    Name: string;
    Slug: string;
    Logo: IImageRef;
}

//

export interface IShopLinks {
    Website: string;
    Blog: string;
    Facebook: string;
    Instagram: string;
    Twitter: string;
    LinkedIn: string;
}

export interface IShopSettings {
	PrimaryContact: IShopSettingsPrimaryContact;
    BusinessDetail: IShopSettingsBusinessDetail;
    BusinessLicense: IShopSettingsBusinessLicense;
    BusinessContact: IShopSettingsBusinessContact;
}

export interface IShopSettingsPrimaryContact {
    NameFirst: string;
    NameLast: string;
    Day: string;
    Month: string;
    Year: string;
    Email: string;
    Phone: string;
    IDFront: IImageRef;
    IDBack: IImageRef;
}

export interface IShopSettingsBusinessDetail {
    Name: string;
    Type: string;
    ID:   string;
}

export interface IShopSettingsBusinessLicense {
    Number: string;
    Designation: string;
    Type: string;
}

export interface IShopSettingsBusinessContact {
	Address1: string;
	Address2: string;
	City: string;
	State: string;
	Country: string;
	Zip: string;
    Email: string;
    Phone: string;
}

export interface IShopSettingsBusinessContact {
    Email: string;
    Phone: string;
}
