import { IImageRef } from '@app/models/_shared/shared.interfaces';

export interface IAccount {
    ID: string;
    Username: string;
    Confirmed: boolean;
    PasswordSet: boolean;
    Avatar: IImageRef;
	Connections: IAccountConnections;
}

export interface IAccountConnections {
	Facebook: boolean;
	Google: boolean;
	LinkedIn: boolean;
}

export interface IAccountToken {
	Value: string;
}
