import { EventEmitter } from '@angular/core';

// events

export let AccountSignedInEvent: EventEmitter<boolean> = new EventEmitter();
