import { ISelectOption } from '@app/services/core/helpers/form.interfaces';

export let OrderOptions: ISelectOption[] = [
    {
        Key: 'received',
        Value: 'Received'
    },
    {
        Key: 'confirmed',
        Value: 'Confirmed'
    },
    {
        Key: 'ready',
        Value: 'Ready'
    },
    {
        Key: 'done',
        Value: 'Done'
    }
];

export let StatusOptions: ISelectOption[] = [
    {
        Key: 'published',
        Value: 'Published'
    },
    {
        Key: 'draft',
        Value: 'Draft'
    },
    {
        Key: 'disabled',
        Value: 'Disabled'
    }
];

export let ConfirmOptions: ISelectOption[] = [
	{
		Key: <boolean>true,
		Value: 'True'
	},
	{
		Key: <boolean>false,
		Value: 'False'
	}
];

// date

export let DataMonths: ISelectOption[] = [
	{
		Key: 1,
		Value: 'January'
	},
	{
		Key: 2,
		Value: 'February'
	},
	{
		Key: 3,
		Value: 'March'
	},
	{
		Key: 4,
		Value: 'April'
	},
	{
		Key: 5,
		Value: 'May'
	},
	{
		Key: 6,
		Value: 'June'
	},
	{
		Key: 7,
		Value: 'July'
	},
	{
		Key: 8,
		Value: 'August'
	},
	{
		Key: 9,
		Value: 'September'
	},
	{
		Key: 10,
		Value: 'October'
	},
	{
		Key: 11,
		Value: 'November'
	},
	{
		Key: 12,
		Value: 'December'
	}
];

// date

export let DataCountries: ISelectOption[] = [
    {
        Key: 'US',
        Value: 'United States'
    },
];

export let DataUSStates: ISelectOption[] = [
    {
        Key: 'WA',
        Value: 'Washington'
    },
    {
        Key: 'OR',
        Value: 'Oregon'
    },
    {
        Key: 'CA',
        Value: 'California'
    },
    {
        Key: 'NV',
        Value: 'Nevada'
    },
    {
        Key: 'CO',
        Value: 'Colorado'
    },
    {
        Key: 'AK',
        Value: 'Alaska'
    },
    {
        Key: 'MI',
        Value: 'Michigan'
    },
    {
        Key: 'VT',
        Value: 'Vermont'
    },
    {
        Key: 'MA',
        Value: 'Massachusetts'
    },
    {
        Key: 'ME',
        Value: 'Maine'
    },
];

export let DataBusinessTypes: ISelectOption[] = [
    {
        Key: 'llc',
        Value: 'LLC'
    },
    {
        Key: 'corporation',
        Value: 'Corporation'
    }
];

export let DataBusinessLicenseTypes: ISelectOption[] = [
    {
        Key: 'breeder',
        Value: 'Breeder'
    },
    {
        Key: 'cultivator',
        Value: 'Cultivator'
    },
    {
        Key: 'manufacturing',
        Value: 'Manufacturing'
    },
    {
        Key: 'distribution',
        Value: 'Distribution'
    },
    {
        Key: 'testing',
        Value: 'Testing'
    },
    {
        Key: 'microbusiness',
        Value: 'Microbusiness'
    },
    {
        Key: 'event-organizer',
        Value: 'Event Organizer'
    }
];

export let DataBusinessLicenseDesignations: ISelectOption[] = [
    {
        Key: 'recreational',
        Value: 'Recreational'
    },
    {
        Key: 'medicinal',
        Value: 'Medicinal'
    },
];

export let DataProductCategoryMain: ISelectOption[] = [
    {
        Key: 'raw',
        Value: 'Raw'
    },
    {
        Key: 'processed',
        Value: 'Processed'
    },
    {
        Key: 'processed',
        Value: 'Processed'
    }
];

// product [all]

export let DataProductTaxonomy: ISelectOption[] = [
    {
        Key: 'cannabis-hemp',
        Value: 'Cannabis or Hemp'
    },
    {
        Key: 'non-cannabis',
        Value: 'Non-Cannabis'
    },
    {
        Key: 'service',
        Value: 'Service'
    },
    {
        Key: 'cannabis',
        Value: 'Cannabis'
    },
    {
        Key: 'hemp-cbd',
        Value: 'Hemp / CBD'
    },
    {
        Key: 'natural',
        Value: 'Natural'
    },
    {
        Key: 'concentrate',
        Value: 'Concentrate'
    },
    {
        Key: 'na',
        Value: 'N/A'
    },
    {
        Key: 'seed',
        Value: 'Seed'
    },
    {
        Key: 'clone',
        Value: 'Clone'
    },
    {
        Key: 'flower',
        Value: 'Flower'
    },
    {
        Key: 'trim-cut',
        Value: 'Trim / Cut'
    },
    {
        Key: 'solvent',
        Value: 'Solvent'
    },
    {
        Key: 'solventless',
        Value: 'Solventless'
    },
    {
        Key: 'tincture',
        Value: 'Tincture'
    },
    {
        Key: 'oil',
        Value: 'Oil'
    },
    {
        Key: 'shatter',
        Value: 'Shatter'
    },
    {
        Key: 'wax',
        Value: 'Wax'
    },
    {
        Key: 'distillate',
        Value: 'Distillate'
    },
    {
        Key: 'crystalline',
        Value: 'Crystalline'
    },
    {
        Key: 'terpene',
        Value: 'Terpene'
    },
    {
        Key: 'kief',
        Value: 'Kief'
    },
    {
        Key: 'hash',
        Value: 'Hash'
    },
    {
        Key: 'resin',
        Value: 'Resin'
    },
    {
        Key: 'natural',
        Value: 'Natural'
    },
    {
        Key: 'processed',
        Value: 'Processed'
    },
    {
        Key: 'seed',
        Value: 'Seed'
    },
    {
        Key: 'flower',
        Value: 'Flower'
    },
    {
        Key: 'biomass',
        Value: 'Biomass'
    },
    {
        Key: 'crude-oil',
        Value: 'Crude Oil'
    },
    {
        Key: 'winter-crude-oil',
        Value: 'Winter Crude Oil'
    },
    {
        Key: 'tinctures',
        Value: 'Tinctures'
    },
    {
        Key: 'resin',
        Value: 'Resin'
    },
    {
        Key: 'wax',
        Value: 'Wax'
    },
    {
        Key: 'shatter',
        Value: 'Shatter'
    },
    {
        Key: 'distillate',
        Value: 'Distillate'
    },
    {
        Key: 'water-soluble-liquid',
        Value: 'Water Soluble Liquid'
    },
    {
        Key: 'crystalline-cbd-water-soluble-liquid',
        Value: 'Crystalline CBD Water Soluble Liquid'
    },
    {
        Key: 'water-soluble-powder',
        Value: 'Water Soluble Powder'
    },
    {
        Key: 'isolate',
        Value: 'Isolate'
    },
    {
        Key: 'accessory',
        Value: 'Accessory'
    },
    {
        Key: 'apparel',
        Value: 'Apparel'
    },
    {
        Key: 'grower-supplly',
        Value: 'Grower Supplly'
    },
    {
        Key: 'other',
        Value: 'Other'
    }
];

// product

export let DataProductCategory: ISelectOption[] = [
    {
        Key: 'cannabis-hemp',
        Value: 'Cannabis or Hemp'
    },
    {
        Key: 'non-cannabis',
        Value: 'Non-Cannabis'
    },
    {
        Key: 'service',
        Value: 'Service'
    },
];

export let DataProductGroupCannabis: ISelectOption[] = [
    {
        Key: 'cannabis',
        Value: 'Cannabis'
    },
    {
        Key: 'hemp-cbd',
        Value: 'Hemp / CBD'
    },
];

export let DataProductClassCannabis: ISelectOption[] = [
    {
        Key: 'natural',
        Value: 'Natural'
    },
    {
        Key: 'concentrate',
        Value: 'Concentrate'
    },
];

export let DataProductMethodCannabisNatural: ISelectOption[] = [
    {
        Key: 'na',
        Value: 'N/A'
    },
];

export let DataProductTypeCannabisNA: ISelectOption[] = [
    {
        Key: 'seed',
        Value: 'Seed'
    },
    {
        Key: 'clone',
        Value: 'Clone'
    },
    {
        Key: 'flower',
        Value: 'Flower'
    },
    {
        Key: 'trim-cut',
        Value: 'Trim / Cut'
    }
];

export let DataProductMethodCannabisConcentrate: ISelectOption[] = [
    {
        Key: 'solvent',
        Value: 'Solvent'
    },
    {
        Key: 'solventless',
        Value: 'Solventless'
    }
];

export let DataProductTypeCannabisSolvent: ISelectOption[] = [
    {
        Key: 'tincture',
        Value: 'Tincture'
    },
    {
        Key: 'oil',
        Value: 'Oil'
    },
    {
        Key: 'shatter',
        Value: 'Shatter'
    },
    {
        Key: 'wax',
        Value: 'Wax'
    },
    {
        Key: 'distillate',
        Value: 'Distillate'
    },
    {
        Key: 'crystalline',
        Value: 'Crystalline'
    },
    {
        Key: 'terpene',
        Value: 'Terpene'
    }
];

export let DataProductTypeCannabisSolventless: ISelectOption[] = [
    {
        Key: 'kief',
        Value: 'Kief'
    },
    {
        Key: 'hash',
        Value: 'Hash'
    },
    {
        Key: 'resin',
        Value: 'Resin'
    }
];

export let DataProductClassHemp: ISelectOption[] = [
    {
        Key: 'natural',
        Value: 'Natural'
    },
    {
        Key: 'processed',
        Value: 'Processed'
    }
];

export let DataProductMethodHempNatural: ISelectOption[] = [
    {
        Key: 'na',
        Value: 'N/A'
    },
];

export let DataProductTypeHempNA: ISelectOption[] = [
    {
        Key: 'seed',
        Value: 'Seed'
    },
    {
        Key: 'flower',
        Value: 'Flower'
    },
    {
        Key: 'biomass',
        Value: 'Biomass'
    }
];

export let DataProductMethodHempProcessed: ISelectOption[] = [
    {
        Key: 'solvent',
        Value: 'Solvent'
    },
];

export let DataProductTypeHempSolvent: ISelectOption[] = [
    {
        Key: 'crude-oil',
        Value: 'Crude Oil'
    },
    {
        Key: 'winter-crude-oil',
        Value: 'Winter Crude Oil'
    },
    {
        Key: 'tinctures',
        Value: 'Tinctures'
    },
    {
        Key: 'resin',
        Value: 'Resin'
    },
    {
        Key: 'wax',
        Value: 'Wax'
    },
    {
        Key: 'shatter',
        Value: 'Shatter'
    },
    {
        Key: 'distillate',
        Value: 'Distillate'
    },
    {
        Key: 'water-soluble-liquid',
        Value: 'Water Soluble Liquid'
    },
    {
        Key: 'crystalline-cbd-water-soluble-liquid',
        Value: 'Crystalline CBD Water Soluble Liquid'
    },
    {
        Key: 'water-soluble-powder',
        Value: 'Water Soluble Powder'
    },
    {
        Key: 'isolate',
        Value: 'Isolate'
    }
];

export let DataProductGroupNonCannabis: ISelectOption[] = [
    {
        Key: 'accessory',
        Value: 'Accessory'
    },
    {
        Key: 'apparel',
        Value: 'Apparel'
    },
    {
        Key: 'grower-supplly',
        Value: 'Grower Supplly'
    },
    {
        Key: 'other',
        Value: 'Other'
    }
];

