export function PopUp(url: string, width: number, height: number): Window {
	let screenLeft = window.screenLeft !== undefined ? window.screenLeft : 0;
	let screenTop = window.screenTop !== undefined ? window.screenTop : 0;

	let popupWidth = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	let popupHeight = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	let left = ((popupWidth / 2) - (width / 2)) + screenLeft;
	let top = ((popupHeight / 2) - (height / 2)) + screenTop;

	return window.open(url, '_blank', 'scrollbars=yes, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left);
}
