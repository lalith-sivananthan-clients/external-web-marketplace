import { DataProductTaxonomy } from '@app/services/core/helpers/form.variables';

export class ProductTaxonomyService {
    public GetTaxonomy(data: string): string {
        for (let i = 0; i < DataProductTaxonomy.length; i++) {
            if (DataProductTaxonomy[i].Key === data) {
                return DataProductTaxonomy[i].Value;
            }
        }

        return '';
    }
}
