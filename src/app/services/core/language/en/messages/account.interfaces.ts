import { IMessage } from '@app/services/core/language/langauge.interfaces';

export interface IAccountMessages {
	ConfirmSuccess: IMessage;
	ConfirmError: IMessage;
	ConfirmEmailSuccess: IMessage;
	ConfirmEmailError: IMessage;
	EmailUpdateSuccess: IMessage;
	EmailUpdateError: IMessage;
	UsernameUpdateSuccess: IMessage;
	UsernameUpdateError: IMessage;
	PasswordUpdateSuccess: IMessage;
	PasswordUpdateError: IMessage;
}
