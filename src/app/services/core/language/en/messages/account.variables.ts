import { IAccountMessages } from './account.interfaces';

import { MessageTypeEnum } from '@app/services/core/language/language.enums';

// account

export const AccountMessages: IAccountMessages = {
	ConfirmError: {
		Show: false,
		Name: 'account-confirm-error',
		Type: MessageTypeEnum.Error,
		Body: 'Unable to confirm your account.'
	},
	ConfirmSuccess: {
		Show: false,
		Name: 'account-confirm-success',
		Type: MessageTypeEnum.Success,
		Body: 'Successfully confirmed your account.'
	},
	ConfirmEmailError: {
		Show: false,
		Name: 'account-confirm-email-error',
		Type: MessageTypeEnum.Error,
		Body: 'Unable to confirm your email.'
	},
	ConfirmEmailSuccess: {
		Show: false,
		Name: 'account-confirm-email-success',
		Type: MessageTypeEnum.Success,
		Body: 'Successfully confirmed your email.'
	},
	EmailUpdateSuccess: {
		Show: false,
		Name: 'account-email-update-success',
		Type: MessageTypeEnum.Success,
		Body: 'Please follow the confirmation link in your email to confirm your new email.'
	},
	EmailUpdateError: {
		Show: false,
		Name: 'account-email-update-error',
		Type: MessageTypeEnum.Error,
		Body: 'Unable to update your email.'
	},
	UsernameUpdateSuccess: {
		Show: false,
		Name: 'account-username-update-success',
		Type: MessageTypeEnum.Success,
		Body: 'Successfully updated your username.'
	},
	UsernameUpdateError: {
		Show: false,
		Name: 'account-username-update-error',
		Type: MessageTypeEnum.Error,
		Body: 'Unable to update your username.'
	},
	PasswordUpdateSuccess: {
		Show: false,
		Name: 'account-password-update-success',
		Type: MessageTypeEnum.Success,
		Body: 'Successfully updated your password.'
	},
	PasswordUpdateError: {
		Show: false,
		Name: 'account-password-update-error',
		Type: MessageTypeEnum.Error,
		Body: 'Unable to update your password.'
	}
};
