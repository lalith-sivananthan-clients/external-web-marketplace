import { IMessage } from '@app/services/core/language/langauge.interfaces';

export interface IGlobalBasicMessages {
	Success: IMessage;
	Error: IMessage;
	Server: IMessage;
}

export interface IGlobalEmailMessages {
	ConfirmCheck: IMessage;
	ConfirmSendError: IMessage;
	ConfirmSendSuccessful: IMessage;
}
