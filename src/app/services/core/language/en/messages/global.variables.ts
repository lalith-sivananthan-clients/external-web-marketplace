import { IGlobalBasicMessages, IGlobalEmailMessages } from './global.interfaces';

import { MessageTypeEnum } from '@app/services/core/language/language.enums';

// basic

export const GlobalBasicMessages: IGlobalBasicMessages = {
	Success: {
		Show: false,
		Name: 'global-basic-success',
		Type: MessageTypeEnum.Success,
		Body: 'Successfully processed your request'
	},
	Error: {
		Show: false,
		Name: 'global-basic-error',
		Type: MessageTypeEnum.Error,
		Body: 'Error processing your request'
	},
	Server: {
		Show: false,
		Name: 'global-basic-server',
		Type: MessageTypeEnum.Error,
		Body: 'Unable to reach server'
	}
};

// email

export const GlobalEmailMessages: IGlobalEmailMessages = {
	ConfirmCheck: {
		Show: false,
		Name: 'global-email-confirm-check',
		Type: MessageTypeEnum.Warning,
		Body: 'Please confirm your email.'
	},
	ConfirmSendError: {
		Show: false,
		Name: 'global-email-confirm-send-error',
		Type: MessageTypeEnum.Error,
		Body: 'Error sending confirmation email.'
	},
	ConfirmSendSuccessful: {
		Show: false,
		Name: 'global-email-confirm-send-successful',
		Type: MessageTypeEnum.Success,
		Body: 'Confirmation email sent.'
	}
};
