import { MessageTypeEnum } from './language.enums';

import { IGlobalBasicMessages, IGlobalEmailMessages } from './en/messages/global.interfaces';
import { IAccountMessages } from './en/messages/account.interfaces';

export interface IMessage {
	Show: boolean;
	Name: string;
	Type: MessageTypeEnum;
	Body: string;
}

export interface ILabel {
	Show: boolean;
	Name: string;
	Body: string;
}

export interface IMessages {
	GlobalBasic: IGlobalBasicMessages;
	GlobalEmail: IGlobalEmailMessages;
	Account: IAccountMessages;
}
