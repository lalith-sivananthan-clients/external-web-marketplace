import { Injectable } from '@angular/core';

@Injectable()
export class PaginationService {

	private _current: number;
	private _total: number;

	constructor() {
		this._current = this._current || 1;
		this._total = this._total || 1;
	}

	Set(current: number, total: number) {
		this._current = current;
		this._total = total;
	}

	Next(): number {
		return this._current + 1;
	}

	NextShow(): boolean {
		return this._current < this._total;
	}

	Previous(): number {
		return this._current - 1;
	}

	PreviousShow(): boolean {
		return this._current > 1;
	}

}
