import { Injectable, Inject, PLATFORM_ID, InjectionToken } from '@angular/core';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';

import { StorageConfig } from '@app/configs/config.consts';

@Injectable()
export class StorageCookieService {

    private _documentIsAccessible: boolean;
    private _domain: string = StorageConfig.Domain;

    constructor(
        @Inject(DOCUMENT) private document: any,
        @Inject(PLATFORM_ID) private platformID: InjectionToken<Object>
    ) {
        this._documentIsAccessible = isPlatformBrowser(this.platformID);
    }

    // func : public

    public Get(key): string {
        // return this.document.cookie;

        if (this._documentIsAccessible && this._check(key)) {
            const name = encodeURIComponent(key);
            const regExp: RegExp = this._getCookieRegEx(name);
            const result: RegExpExecArray = regExp.exec(this.document.cookie);

            return decodeURIComponent(result[1]);
        } else {
            return '';
        }
    }

    public Save(key: string, data: any): void {
        if (! this._documentIsAccessible) {
            return;
        }

        let cookie: string = encodeURIComponent(key) + '=' + encodeURIComponent(data) + ';';

        cookie += 'path=/;';
        cookie += 'domain=' + this._domain + ';';

        this.document.cookie = cookie;
    }

    public Destroy(key: string): void {
        if (! this._documentIsAccessible) {
            return;
        }

        this.Save(key, '');
    }

    public DestroyAll(): void {
        if (! this._documentIsAccessible) {
            return;
        }

        this.document.cookie = '';
    }

    // func : private

    _check(key: string): boolean {
        if (! this._documentIsAccessible) {
            return false;
        }

        const name = encodeURIComponent(key);
        const regEx: RegExp = this._getCookieRegEx(name);

        return regEx.test(this.document.cookie);
    }

    private _getCookieRegEx(key: string): RegExp {
        const escaped: string = key.replace(/([\[\]\{\}\(\)\|\=\;\+\?\,\.\*\^\$])/ig, '\\$1');

        return new RegExp('(?:^' + escaped + '|;\\s*' + escaped + ')=(.*?)(?:;|$)', 'g');
    }
}
