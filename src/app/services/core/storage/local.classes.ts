import { Injectable } from '@angular/core';

import { StorageConfig } from '@app/configs/config.consts';

@Injectable()
export class StorageLocalService {

    private storage: any;
    private name: string;

    constructor() {
        this.name = StorageConfig.Name;

        if ('localStorage' in window && window.localStorage !== null) {
            this.storage = localStorage;
        } else {
            console.error('Local Storage is disabled or unavailable.');
        }
    }

    // func : public

    public Get(key) {
        let _key  = this.name + '.' + key;
        let _data = this.storage.getItem(_key);

        try {
            return JSON.parse(_data);
        } catch (e) {
            return _data;
        }
    }

    public Save(key, data) {
        let _key  = this.name + '.' + key;
        let _data = null;

        if (typeof data === 'object' || Array.isArray(data) === true) {
            _data = JSON.stringify(data);
        } else {
            _data = data;
        }

        return this.storage.setItem(_key, _data);
    }

    public Destroy(key) {
        let _key = this.name + '.' + key;

        return this.storage.removeItem(_key);
    }
}
