import { ValidatorRuleEnum, ValidatorOptionEnum } from './validator.enums';

export interface IValidatorRule {
	Name: ValidatorRuleEnum;
	Options: IValidatorOption[];
}

export interface IValidatorOption {
	Name: ValidatorOptionEnum;
	Value: any;
}
