import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { AccountModel } from '@app/models/local/account/account.classes';

import { MessagerService } from '@app/services/core/messenger/messenger.classes';

import { URLConfig } from '@app/configs/config.consts';

@Injectable()
export class AuthenticationGuard implements CanActivate {

    constructor(
        private router: Router,
        private accountModel: AccountModel,
        private messagerService: MessagerService
    ) {
        this.accountModel.Check();
    }

    canActivate() {
        if (this.accountModel.IsLoggedIn() === true) {
            this.messagerService.CheckGlobal();

            return true;
        }

        document.location.href = URLConfig.WebAccount;
    }

}
